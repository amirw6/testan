import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-savedposts',
  templateUrl: './savedposts.component.html',
  styleUrls: ['./savedposts.component.css']
})
export class SavedpostsComponent implements OnInit {

  
  
  posts$:Observable<any>;
  userId:string;
  

  removePost(id:string){
    console.log(id)
    this.PostsService.deletePost(this.userId,id);
  }

  constructor(private PostsService:PostsService,public authService:AuthService) { 

  }
  addLikes(id:string,likes:number){
     likes++;
    this.PostsService.addLike(this.userId,id,likes)
    //this.router.navigate(['savedposts'])
  }

  ngOnInit() {
   
      this.authService.user.subscribe(
        user=>{
          this.userId = user.uid;
          this.posts$ = this.PostsService.getPosts(this.userId);
        }
      )

  }

}
