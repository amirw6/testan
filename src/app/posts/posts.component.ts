import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { AuthService } from '../auth.service';
import { HttpBackend } from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {


    public posts: any;
    public comments: any;
    userId:string;
    save:string;
    onlyfor:number;
  

  constructor(public PostsService:PostsService, public authService:AuthService, private http:HttpBackend) { 

  }


  savePost(id:number,title:string, body:string){

    this.onlyfor = id;
    this.save = "Saved for Later viewing"

      this.PostsService.addPost(this.userId,id,title,body)
  }


  ngOnInit() {

    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )

    this.PostsService.getPostsandComments().
     subscribe(data =>{
        this.posts = data[0];
        this.comments = data[1];
         console.log(this.posts);
         console.log(this.comments);
        
      });
  }

}
