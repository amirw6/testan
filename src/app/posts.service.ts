import { Posts } from './interfaces/posts';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private PostURL = "https://jsonplaceholder.typicode.com/posts/";
  private CommentURL = "https://jsonplaceholder.typicode.com/comments/"

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection: AngularFirestoreCollection;
  
  constructor(private http:HttpClient,
              private db:AngularFirestore) { }
  
                    getPostsandComments(): Observable<any[]> {
                      let getPosts = this.http.get(this.PostURL);
                      let getComments = this.http.get(this.CommentURL);
                      
                      // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6
                      return forkJoin([getPosts, getComments]);
                    }

                    addPost(userId:string, idPost:number,title:string, body:string){
                      const post = {idPost:idPost,title:title, body:body ,likes:0}
                      //this.db.collection('books').add(book);
                      this.userCollection.doc(userId).collection('posts').add(post);
                    }

                    deletePost(userId:string,id:string){
                          this.db.doc(`users/${userId}/posts/${id}`).delete()
                    }


                    addLike(userId:string,id:string,likes:number){
                      this.db.doc(`users/${userId}/posts/${id}`).update({
                        likes:likes}
                      )
                      }

                    getPosts(userId:string):Observable<any[]>{
                      
                      this.postCollection = this.db.collection(`users/${userId}/posts`);
                      return this.postCollection.snapshotChanges().pipe(
                        map(
                          collection => collection.map(
                            document => {
                              const data = document.payload.doc.data();
                              data.id = document.payload.doc.id;
                              return data;
                            }
                          )
                    
                        )
                      )
                    }


                    







}
 

