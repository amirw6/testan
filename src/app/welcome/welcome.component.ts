import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authService:AuthService) { }
  
  useremail:any;

  ngOnInit() {
   this.authService.user.subscribe(
     user=>{
       this.useremail = user.email
     }
   )
   console.log(this.useremail)
  }

}
