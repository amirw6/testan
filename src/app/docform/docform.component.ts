import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  constructor(private classifyservice:ClassifyService,
    private router:Router) { }
text:string;

  ngOnInit() {
  }
  onSubmit(){
    this.classifyservice.doc = this.text;
    this.router.navigate(['/classified']);
  }


}
