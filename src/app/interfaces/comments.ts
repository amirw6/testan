export interface Comments {
    postId:number, 
    id:number,
    name:string,
    email:String,
    body:String
}
