// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebaseConfig : {
    apiKey: "AIzaSyAAjpeU2uzVWKnU9Hs0tH3M5DhD7RycaUs",
    authDomain: "testan-ea477.firebaseapp.com",
    databaseURL: "https://testan-ea477.firebaseio.com",
    projectId: "testan-ea477",
    storageBucket: "testan-ea477.appspot.com",
    messagingSenderId: "454817327110",
    appId: "1:454817327110:web:5c3b59dbc5fcc5277bfb87"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
